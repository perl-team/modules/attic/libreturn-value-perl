Source: libreturn-value-perl
Maintainer: Debian Perl Group <pkg-perl-maintainers@lists.alioth.debian.org>
Uploaders: Krzysztof Krzyzaniak (eloy) <eloy@debian.org>,
           gregor herrmann <gregoa@debian.org>,
           Nathan Handler <nhandler@debian.org>,
           Florian Schlichting <fsfs@debian.org>
Section: perl
Priority: optional
Build-Depends: debhelper (>= 8)
Build-Depends-Indep: perl
Standards-Version: 3.9.5
Vcs-Browser: http://anonscm.debian.org/gitweb/?p=pkg-perl/packages/libreturn-value-perl.git
Vcs-Git: git://anonscm.debian.org/pkg-perl/packages/libreturn-value-perl.git
Homepage: https://metacpan.org/release/Return-Value

Package: libreturn-value-perl
Architecture: all
Depends: ${perl:Depends},
         ${misc:Depends}
Description: Perl module for polymorphic return values
 Polymorphic return values are really useful.  Often, we just want to know if
 something worked or not.  Other times, we'd like to know what the error text
 was.  Still others, we may want to know what the error code was, and what the
 error properties were.  We don't want to handle objects or data structures for
 every single return value, but we do want to check error conditions in our code
 because that's what good programmers do.
 .
 When functions are successful they may return true, or perhaps some useful
 data.  In the quest to provide consistent return values, this gets confusing
 between complex, informational errors and successful return values.
 .
 Return::Value provides these features with a simple API that should get you
 what you're looking for in each context a return value is used in.
 .
 WARNING: THIS MODULE IS DEPRECATED. IT HAS STARTED TO ISSUE WARNINGS WHEN
          USED, AND WILL GO AWAY EVENTUALLY. DO NOT USE!

